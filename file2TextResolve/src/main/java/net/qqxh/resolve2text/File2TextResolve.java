package net.qqxh.resolve2text;

public interface File2TextResolve {
    String resolve(byte[] data) ;
    String getType();
}
