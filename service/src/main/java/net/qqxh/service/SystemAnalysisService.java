package net.qqxh.service;

import net.qqxh.persistent.SearchLib;

/**
 * Created by jason on 2019/2/13 14:40
 */
public interface SystemAnalysisService {
    /**
     * 查询redis是否启动
     * @return
     */
    boolean getRedisIsStart();

    /**
     * 查询es是否启动
     * @return
     */
    boolean getEsIsStart();

    /**
     * 检测系统是否格式化
     * @return
     */
    boolean getSystemIsInit(SearchLib searchLib);
}
