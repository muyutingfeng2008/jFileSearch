package net.qqxh.service;

import net.qqxh.persistent.Jfile;
import net.qqxh.persistent.SearchLib;

import java.io.IOException;

public interface FileResolveService {
    String resolveFile2Text(byte[] file, String type);



    boolean resolve(SearchLib searchLib, Jfile file) throws IOException;

    String resolveFile2View(String filePath, SearchLib searchLib, String type);

    void deleteFileFromRedis(String rid);
}
