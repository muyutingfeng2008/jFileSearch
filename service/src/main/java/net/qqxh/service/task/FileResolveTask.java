package net.qqxh.service.task;

import net.qqxh.persistent.Jfile;
import net.qqxh.persistent.SearchLib;
import net.qqxh.service.FileResolveService;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class FileResolveTask {
    @Autowired
    FileResolveService fileResolveService;

    private final static Logger logger = LoggerFactory.getLogger(FileResolveTask.class);

    @Async("fileResolveExecutor")
    public void doFileResolve(SearchLib searchLib, Jfile file, FileResolveTaskCallBack fileResolveTaskCallBack, Integer totalCount) throws IOException {
        Thread th=Thread.currentThread();
        logger.info(System.currentTimeMillis()+"................."+th.getId()+".................."+th.getName());
        long start = System.currentTimeMillis();
        String path = file.getPath();
        Map msg = new HashMap<>();
        msg.put("path", path);
        msg.put("totalCount", totalCount);
        try {
            fileResolveService.resolve(searchLib,file);
            long end = System.currentTimeMillis();
            logger.info("转换文件==《" + path + "'》==成功，耗时：" + (end - start) + "毫秒");
            msg.put("timeCost", end - start);
            msg.put("msg", "成功");
        } catch (Exception e) {
            long end = System.currentTimeMillis();
            logger.warn("转换文件==《" + path + "'》==失败，耗时：" + (end - start) + "毫秒,{}",e.fillInStackTrace());
            msg.put("timeCost", 0);
            msg.put("msg", "失败");
        }
        if(fileResolveTaskCallBack!=null){
            fileResolveTaskCallBack.doCallBack(msg);
        }

    }


}
