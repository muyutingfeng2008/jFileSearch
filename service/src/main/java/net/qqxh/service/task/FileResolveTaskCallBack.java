package net.qqxh.service.task;

import java.util.Map;

public interface FileResolveTaskCallBack {
    public void doCallBack(Map<String, String> msg);
}
