package net.qqxh.service.impl;

import net.qqxh.persistent.SearchLib;
import net.qqxh.service.SystemAnalysisService;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsRequest;
import org.elasticsearch.action.admin.indices.exists.indices.IndicesExistsResponse;
import org.elasticsearch.client.transport.TransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.stereotype.Service;

import java.net.ConnectException;
import java.util.List;

/**
 * Created by 18060774 on 2019/2/13 14:54
 */
@Service
public class SystemAnalysisServiceImpl implements SystemAnalysisService {
    @Autowired
    private JedisConnectionFactory jedisConnectionFactory;
    @Autowired
    private TransportClient esclient;

    @Override
    public boolean getRedisIsStart() {
        try {
          /*  return !jedisConnectionFactory;*/
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean getEsIsStart() {
        try {
            List list = esclient.connectedNodes();
            if (list.size() > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }

    }

    @Override
    public boolean getSystemIsInit(SearchLib searchLib) {
        IndicesExistsRequest request = new IndicesExistsRequest(searchLib.getEsIndex());
        IndicesExistsResponse response = esclient .admin().indices().exists(request).actionGet();
        if (response.isExists()) {
            return true;
        }
        return false;
    }

}
