var singleFileResolveApp = (function () {
    var unique;

    function getInstance() {
        if (unique != undefined) {
            return unique;
        } else {
            unique = new fileResolveApp();
        }
        return unique;
    }

    var fileResolveApp = function (option) {
        this.$url = "fileResolveApp.html";
        this.$window = null;
        this.$windowContent = null;
        this.$isrun = false;

    }

    fileResolveApp.prototype = {
        init: function () {
            if (this.$isrun) {
                this.$window.tag_hover_window();
                return;
            }
            this.$isrun = true;
            this.$window = new Window(1, 450, 566, "系统向导", {closeCallback: this.stop_app.bind(this)});
            this.$window.initialize();
            var _this = this;
            this.$windowContent = this.$window.windowContent;
            $.get(_this.$url, function (r) {
                $(_this.$windowContent).html(r);
                _this.v2m();
            }, "html");
        }
        ,
        stop_app: function () {
            screenLog.destroy();
            if (unique) {
                unique = undefined;
            }
        },
        doAjax: function (url, instance,fn) {
            $.ajax({
                url: url,
                type: 'get',
                data: {},
                cache: false,
                success: function (res) {

                    resResolve(res);
                    instance.stop();
                    if(typeof  fn ==="function"){
                        fn();
                    }

                },
                error: function (res) {

                    resResolve(res);
                    instance.stop();

                }
            });
        },

        v2m: function () {
            screenLog.init({
                bgColor: '#fff',
                logColor: '#aaa',
                infoColor: 'blue',
                warnColor: 'orange',
                errorColor: 'red',
                freeConsole: false,
                autoScroll: true,
                showEl: this.$windowContent,
                css: 'max-height:500px;'
            });
            var _this = this;
            /*Ladda.bind('.fileResolveApp button.formatSystem',
                {
                    callback: function (instance) {
                        _this.doAjax('/deleteIndex', instance,function () {
                            _this.doAjax('/cindex', instance);
                        });
                    }
                });*/
            Ladda.bind('.fileResolveApp button.refreshCache',
                {
                    callback: function (instance) {
                        _this.doAjax('/refreshCache', instance,function () {

                        });
                    }
                });

            Ladda.bind('.fileResolveApp button.svncheckout',
                {
                    callback: function (instance) {
                        _this.doAjax('/svn/checkout', instance)
                    }
                });


            Ladda.bind('.fileResolveApp button.formatSystem', {
                callback: function (instance) {
                    var progress = 0;
                    var nowCount = 0;
                    var jsocket = new JWebSocket({
                        uri: 'jfileSocketServer/resolve',
                        projectName: '',
                        message: function (msg) {
                            var data = $.parseJSON(msg.data);
                            if(data.totalCount){
                                var totalCount = data.totalCount;
                                nowCount++;
                                progress = nowCount / totalCount;
                                instance.setProgress(progress);
                                if (progress === 1) {
                                    jsocket.close();
                                    instance.stop();
                                }
                                console.log(nowCount + ":《" + data.path + "》时间成本：" + data.timeCost + "毫秒")
                            }else{
                                console.log(data.msg)
                            }


                        }
                    });
                    jsocket.open_connet();


                }
            });
         /*   Ladda.bind('.fileResolveApp button.clean-viewdir', {
                callback: function (instance) {
                    var progress = 0;
                    var nowCount = 0;
                    var jsocket = new JWebSocket({
                        uri: 'jfileSocketServer/clean',
                        projectName: '',
                        message: function (msg) {
                            var data = $.parseJSON(msg.data);
                            var totalCount = data.totalCount;
                            nowCount++;
                            progress = nowCount / totalCount;
                            instance.setProgress(progress);
                            if (progress === 1) {
                                jsocket.close();
                                instance.stop();
                            }
                            if(data.path != null) {
                                if(data.status == 0) {
                                    console.warn(data.path + "删除失败");
                                } else if(data.status == 1) {
                                    console.info(data.path + "删除成功");
                                } else if(data.status == 2) {
                                    console.log(data.path);
                                }
                            }
                            if(data.timeCost != null) {
                                console.log("清理完成，耗时：" + data.timeCost + "毫秒");
                            }
                        }
                    });
                    jsocket.open_connet();
                }
            });*/
        }

    }
    return {
        getInstance: getInstance
    }
})();


