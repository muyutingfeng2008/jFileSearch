/**
 * @description
 *        small popup menu.
 * @deprecated
 *        JQuery.js
 * @author Malt
 * @version 1.0
 * Date: 2013-05-22
 */
(function ($, undefined) {
    $.fn.popupSmallMenu = function (options) {
        var defaultOptions = {
                event: null
            },
            options = $.extend(defaultOptions, options);
        /*创建视图*/
        var $currMenu = $("<ul/>").addClass("small-menu")
        $(options.menus).each(function (index, ele) {
            $currMenu.append(
                $("<li class='test'/>").append( $("<a href='javascript:void (0)'  >"+ele.name+"</a>")).click(function () {
                    ele.onMenuClick();
                })
            )
        });
        $("body").append($currMenu);
        var _smallMenu = {
            popupSmallMenu: function () {
                this._bindMenuEvent();
                this._showMenu();
                return $currMenu;
            },
            _bindMenuEvent: function () {
                var thiz = this;
                $currMenu.hover(function () {
                }, function () {
                    $currMenu.remove();
                });

                $currMenu.click(function () {
                    $currMenu.hide();
                });
            },
            _showMenu: function () {

                if (!options.event) {
                    alert('请传入鼠标事件');
                }
                $currMenu.css({
                    top: options.event.clientY + "px",
                    left: (options.event.clientX-5) + "px",
                    display: "block"
                });
            }
        };
        return _smallMenu.popupSmallMenu();
    }
})(jQuery);

