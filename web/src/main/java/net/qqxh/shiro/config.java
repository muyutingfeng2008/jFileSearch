package net.qqxh.shiro;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.io.IOUtils;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.permission.PermissionResolver;
import org.apache.shiro.mgt.RememberMeManager;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import javax.servlet.Filter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class config {
    //将自己的验证方式加入容器
    @Bean
    public JfRealmSimple myJfRralmSimple() {
        JfRealmSimple jfRralmSimple = new JfRealmSimple();
       /* jfRralmSimple.setCredentialsMatcher(credentialsMatcher());*/
        return jfRralmSimple;
    }

    @Bean
    public CredentialsMatcher credentialsMatcher() {
        CredentialsMatcher credentialsMatcher=new HashedCredentialsMatcher();
        ((HashedCredentialsMatcher) credentialsMatcher).setHashAlgorithmName("MD5");
        ((HashedCredentialsMatcher) credentialsMatcher).setHashIterations(1024);
        ((HashedCredentialsMatcher) credentialsMatcher).setStoredCredentialsHexEncoded(true);
        return credentialsMatcher;
    }

    //权限管理，配置主要是Realm的管理认证
    @Bean
    public DefaultWebSecurityManager securityManager() {
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(myJfRralmSimple());
        securityManager.setRememberMeManager(rememberMeManager());
        return securityManager;
    }
    @Bean
    public RememberMeManager rememberMeManager() {
        CookieRememberMeManager rememberMeManager = new CookieRememberMeManager();
        SimpleCookie simpleCookie = new SimpleCookie("rememberMe");
        simpleCookie.setHttpOnly(true);
        simpleCookie.setMaxAge(604800);

        rememberMeManager.setCookie(simpleCookie);
        return rememberMeManager;
    }
    //Filter工厂，设置对应的过滤条件和跳转条件
    @Bean
    public ShiroFilterFactoryBean shiroFilterFactoryBean(DefaultWebSecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        Map<String, Filter> filtersMap = new LinkedHashMap<String, Filter>();
        CustomRolesAuthorizationFilter customRolesAuthorizationFilter = new CustomRolesAuthorizationFilter();

        filtersMap.put("roles",customRolesAuthorizationFilter);
        shiroFilterFactoryBean.setFilters(filtersMap);
        shiroFilterFactoryBean.setSecurityManager(securityManager);
        //登录
        shiroFilterFactoryBean.setLoginUrl("/login");
        //首页
        shiroFilterFactoryBean.setSuccessUrl("/");
        //错误页面，认证不通过跳转
        shiroFilterFactoryBean.setUnauthorizedUrl("/error");
        /*过滤器，分为授权相关的和认证相关的*/
        Map<String, String> map = getPermissions();
        shiroFilterFactoryBean.setFilterChainDefinitionMap(map);
        return shiroFilterFactoryBean;
    }

    //加入注解的使用，不加入这个注解不生效
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(DefaultWebSecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }

    public Map getPermissions() {
        Map<String, String> permissions = new HashMap<>();
        ;
        try {
            ClassPathResource resource = new ClassPathResource("realmDB.json");
            InputStream inputStream = resource.getInputStream();
            String realmDBStr = IOUtils.toString(inputStream, "UTF-8");
            JSONObject jsonObject = JSONObject.parseObject(realmDBStr);
            JSONArray permissionsData = (JSONArray) jsonObject.get("permissions");
            for (Object o : permissionsData) {
                JSONObject j = (JSONObject) o;
                permissions.put(j.getString("url"), j.getString("permission"));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return permissions;
    }
}

